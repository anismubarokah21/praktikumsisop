# nomor 1
### Resource Monitoring

### RAM 

cek status penggunaan RAM dapat menggunakan command `free`, pada command `free` satuan yang ditampilkan adalah kilobyte, jika ingin mengubahnya ke megabyte dapat menggunakan command `free --mega`, jika ingin mengubahnya ke gigabyte dapat menggunakan `free -h`
![](https://gitlab.com/anismubarokah21/psisopp/-/raw/main/ss/ram.PNG)

### CPU

Untuk memonitor penggunaan CPU dapat menggunakan command `top` atau `ps –aux`. perbedaannya jika menggunakan top itu secara real time sedangkan `ps –aux` itu resource eksekusi waktu command aja.
contoh command `top`:
![](https://gitlab.com/anismubarokah21/psisopp/-/raw/main/ss/cpu_1.PNG)

contoh command `top ps -aux`:
![](https://gitlab.com/anismubarokah21/psisopp/-/raw/main/ss/cpu_2.PNG)

### Hardisk/Storage

Untuk memonitor penggunaan storage dapat menggunakan command `lsblk` atau `df`. Perbedaannya, `lsblk` itu mengetahui informasi partisi dan sistem file yang sudah dipasang sebelum di mount, sedangkan `df` itu mengetahui penggunaan disk dan partisi yang sudah di mount pada sistem.
![](https://gitlab.com/anismubarokah21/psisopp/-/raw/main/ss/storage.PNG)

# nomor 2
### Manajemen Program

- Memonitor Program yang sedang berjalan
untuk memonitor program yang sedang berjalan dapat menggunakan command `top` atau `ps -aux`. contoh command `top`:
![](https://gitlab.com/anismubarokah21/psisopp/-/raw/main/ss/cpu_1.PNG)

- Menghentikan Program yang sedang berjalan

untuk menghentikan program yang sedang berjalan yaitu dengan cara masukan command `kill (PID)`

- Otomasi Perintah dengan shell script

untuk mengotomatisasi perintah dengan ShellScript, pertama kita perlu membuat file ShellScript dengan command `touch namafile.sh` lalu masukan perintah `vim` untuk masuk ke dalam file tersebut dan inputkan kode `#! /bin/bash/`
![](https://gitlab.com/anismubarokah21/psisopp/-/raw/main/ss/2.1.PNG)

![](https://gitlab.com/anismubarokah21/psisopp/-/raw/main/ss/2.2.PNG)

untuk menjalankan ShellScript nya ketikkan` ./<namafile>.sh` 
jika muncul output permission denied, maka kita perlu mengubah hak akses file tersebut dengan menggunakan `chmod`, pertama command `ls -l` kemudian `chmod (option)`. chmod diikuti dengan 3 angka, 3 angka tersebut secara berututan mempresentasikan permission dari owner, grup, dan world. nilai dari read(r) = 4, write (w) = 2, dan execute (e) = 1.
![](https://gitlab.com/anismubarokah21/psisopp/-/raw/main/ss/2.3.PNG)


- Penjadwalan Eksekusi Menggunakan cron

untuk menjadwalkan eksekusi program kita bisa menggunakan comman `crontab -e`.
![](https://gitlab.com/anismubarokah21/psisopp/-/raw/main/ss/cron.PNG)

# nomor 3
### Manajemen Network

- Mengakses Sistem Operasi pada jaringan menggunakan SSH

untuk mengakses sistem operasi pada SSH, kita dapat login secara manual pada ssh dan mengetikkan nama server lalu passwordnya.
![](https://gitlab.com/anismubarokah21/psisopp/-/raw/main/ss/ssh.PNG)


- Mampu Memonitor Program yang menggunakan Network

untuk dapat memonitor network kita dapat menggunakan command `netstat`. 
![](https://gitlab.com/anismubarokah21/psisopp/-/raw/main/ss/netstat.PNG)


- Mampu Mengoperasikan HTTP client

untuk melihat operasi pada HTTP client kita dapat memnggunakan command `curl`.
![](https://gitlab.com/anismubarokah21/psisopp/-/raw/main/ss/curl.PNG)

untuk mendownload gambar dapat menggunakan command `wget`.
![](https://gitlab.com/anismubarokah21/psisopp/-/raw/main/ss/wget.PNG)

# nomor 4
### Manajemen File dan Folder

- Navigasi

untuk navigasi dapat menggunakan command `cd`, `ls`, `pwd`.command `cd` untuk mengganti direktori. command `ls` untuk menampilkan semua list file dan folder yang ada di direktori. command `pwd` untuk mengetahui keberadaan direktori sekarang.
![](https://gitlab.com/anismubarokah21/psisopp/-/raw/main/ss/cd.PNG)


- CRUD File dan Folder

Penggunaan editor
untuk editor kita dapat menggunakan command `mkdir` untuk membuat folder, command `touch` untuk membuat file, command `mv` untuk mengubah nama file, command `rmdir` untuk menghapus folder, dan command `rm` untuk menghapus file.
![](https://gitlab.com/anismubarokah21/psisopp/-/raw/main/ss/mkdir.PNG)

otomasi CRUD
untuk otomasi dapat menggunakan ShellScript langkah-langkahnya sama seperti nomor 2.
![](https://gitlab.com/anismubarokah21/psisopp/-/raw/main/ss/ls_-l.PNG)


- Manajemen Ownership dan akses file dan folder

untuk manajemen ownership dan akses file dan folder, kita dapat melihatnya terlebih dahulu menggunakan command `ls -l`, untuk mengganti permission menggunakan command `chmod (option) namafile`. nilai dari read (r) = 4, write (w) = 2, execute (e) = 1, jadi bagian option dapat diganti angka sesuai kebutuhan.
![](https://gitlab.com/anismubarokah21/psisopp/-/raw/main/ss/-l.PNG)

tampilan setelah mengganti permission
![](https://gitlab.com/anismubarokah21/psisopp/-/raw/main/ss/-l.PNG)

- Pencarian File dan Folder

untuk mencari file dan folder dapat menggunakan command `find`.
![](https://gitlab.com/anismubarokah21/psisopp/-/raw/main/ss/find.PNG)

- Kompresi Data 

untuk melakukan kompresi data dapat menggunakan command `tar` atau `gzip`, setelah diubah formatnya dibelakang nama file akan muncul .tar
![](https://gitlab.com/anismubarokah21/psisopp/-/raw/main/ss/tar.PNG)

# nomor 5
Mampu membuat program berbasil CLI menggunakan ShellScript tampilan vim
![](https://gitlab.com/anismubarokah21/psisopp/-/raw/main/ss/vim.PNG)

tampilannya
![](https://gitlab.com/anismubarokah21/psisopp/-/raw/main/ss/vim_1.PNG)

# nomor 6
Mendemonstrasikan Program CLI yang dibuat kepada publik dalam bentuk Youtube

[Link Yt](https://youtu.be/OhfM97Bh8Uk)

# nomor 7
Maze Challenge
![](https://gitlab.com/anismubarokah21/psisopp/-/raw/main/ss/g1.PNG)

![](https://gitlab.com/anismubarokah21/psisopp/-/raw/main/ss/g2.PNG)

![](https://gitlab.com/anismubarokah21/psisopp/-/raw/main/ss/g3.PNG)

untuk selanjutnya bisa di coba game nya pada [LINK](https://maze.informatika.digital/maze/tunet/)
